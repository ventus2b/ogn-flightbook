package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"syscall"

	"github.com/go-redis/redis"
)

const (
	InfoColor    = "\033[1;34m%s\033[0m"
	OkColor      = "\033[1;32m%s\033[0m"
	Redis_addr   = ":6379"
	Redis_stream = "SEVT"
	Wserv_addr   = ":8010"
	Rlimit       = 20000
)

var (
	ctx = context.Background()
)

func main() {
	/*
		Increase hardware resources limitations to allow more connections
		Soft limit Nb_file generally only 1024 (ulimit -aS/ ulimit -aH)
	*/
	fmt.Printf(InfoColor, "Increase soft limit syscall")
	var rLimit syscall.Rlimit
	if err := syscall.Getrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}
	rLimit.Cur = Rlimit
	if err := syscall.Setrlimit(syscall.RLIMIT_NOFILE, &rLimit); err != nil {
		panic(err)
	}
	fmt.Printf(OkColor, "[Ok]\n")
	fmt.Printf(InfoColor, "Initiate Redis connection...")
	rdb := init_redis()
	fmt.Printf(OkColor, "[Ok]\n")
	pool := NewPool()
	go pool.Start()
	fmt.Printf(InfoColor, "Launching Redis stream reader\n")
	go xread(rdb, pool)
	fmt.Printf(InfoColor, "Sarting web serv ...\n")
	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		serveWs(rdb, pool, w, r)
	})
	log.Fatal(http.ListenAndServe(Wserv_addr, nil))
}

func serveWs(rdb *redis.Client, pool *Pool, w http.ResponseWriter, r *http.Request) {
	log.Println("WebSocket Endpoint Hit")
	conn, err := Upgrade(w, r)
	if err != nil {
		log.Printf("%+v\n", err)
		return
	}

	client := &Client{
		Conn: conn,
		Pool: pool,
		Red:  rdb,
	}

	pool.Register <- client
}
