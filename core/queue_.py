import json
from datetime import datetime
from collections import deque
import logging


L_SPEED = 50           # constant low speed detect
L_ALT = 80             # constant low alt detect
INTERVAL = 1800        # constant max time analyze interval in s
STATUS_DELAY = 300     # constant max time status analyze interval in s
RADIUS = 3             # constant RADIUS for airfield detection in km
codes = None


def log_evt(evt, buf):
    logging.info("--------------------")
    logging.info(evt)
    for _ in buf:
        logging.debug("{}".format(_))


def feed_queue(red, delete=True):
    """
    populate the redis QBA queue
    :param red: a redis connection
    :param delete: boolean
    :return:
    """
    # feed the QBA queue only when empty
    if red.exists('QBA') == 0:
        set_ = red.smembers(name='SQBA')
        if set_:
            if delete:
                # TODO add a watch / multi lock mechanism
                red.delete('SQBA')
            red.rpush('QBA', *set_)


def consum_queue(red, db, delete=True):
    """
    Consum the QBA & APRS queue => create & insert event in sql ddb.
    An event is a take_off or landing. Event is insert in sql database
    :param red: a redis connection
    :param db: database object
    :param delete: delete / not delete aprs qeue (use in debug mode)
    :return:
    """
    # codes is a dictionary codes['airfield_oaci_code'] = airfield_elevation, time_zone index
    # push codes in memory and not solicit sql db each time function is call
    global codes
    if codes is None:
        codes = db.get_codes()
    # left pop and block until QBA not empty
    _, key = red.blpop('QBA')
    records = [json.loads(elt) for elt in red.lrange(name=key, start=0, end=-1)]
    # we need at least 4 elts for analyse
    if len(records) < 4:
        return

    # trim the queue as soon as possible, to avoid aprs messages lost
    if delete:
        # always keep the three last beacons for next run
        red.ltrim(key, -3, -1)

    address = str(key)[3:]
    stream = 'SA:{}'.format(address)
    polyline = list()

    # for polyline, get the latest tsp stored in redis stream
    last_tsp = 0
    xrr = red.xrevrange(name=stream, count=1)
    if xrr is not None and len(xrr) == 1:
        _, fields = xrr[0]
        tsp_ = fields.get('tsp', None)
        last_tsp = int(tsp_) if tsp_ is not None else 0

    # get the latest code, timestamp, max_level & last_event stored in redis 'STATUS' key
    # last_event: 0 -> formally detected landed on known airfield
    #           : 1 -> formally detected takeoff from known airfield
    #           : 2 -> formally on ground on known airfield
    #           : 3 -> aircraft stop on unknown position
    jstatus = red.hget(name='STS', key=address)
    status = json.loads(jstatus) if jstatus is not None else {'c': None, 't': 0, 'ml': 0, 'le': -1}

    buf = deque((), 4)
    old_lspeed = False
    first = True

    for index_ in range(len(records)):
        cur_fix = records[index_]
        # evaluate the low_speed flag
        cur_fix['low_speed'] = cur_fix['gsp'] < L_SPEED

        # update the max elevation
        if cur_fix['alt'] > status['ml']:
            status['ml'] = cur_fix['alt']

        buf.append(cur_fix)

        # append new val for polyline only after 4th fix
        if len(buf) == 4:
            # append fix on polyline only for given time delta
            if buf[-1]['tsp'] - last_tsp > 20:
                # append fix on polyline only for new positions
                if buf[-1]['lat'] != buf[-2]['lat'] and buf[-1]['lng'] != buf[-2]['lng']:
                    polyline.append((round(buf[-1]['lat'], 5), round(buf[-1]['lng'], 5)))
                    last_tsp = buf[-1]['tsp']

        # go deeper only if two consecutive last point have the same low_speed flag
        if not first and cur_fix['low_speed'] != old_lspeed:
            old_lspeed = cur_fix['low_speed']
            continue
        old_lspeed = cur_fix['low_speed']
        first = False

        # we need at least 4 points for event analyze
        if len(buf) < 4:
            continue

        # test aircraft possibly on ground
        is_ground = (buf[1]['gsp'] == buf[2]['gsp'] == 0)

        # go deeper only if two consecutive first point have the same low_speed flag or is_ground
        if buf[0]['low_speed'] != buf[1]['low_speed'] and not is_ground:
            continue

        # invalidate buffer for time > INTERVAL unless is_ground
        if buf[3]['tsp'] - buf[0]['tsp'] > INTERVAL and not is_ground:
            buf.clear()
            first = True
            red.hset('MLV', address, 0)
            continue

        prev_fix = buf[1]
        next_fix = buf[2]

        if is_ground:
            # Do not process for aircraft formally detected as landed or formally detected on airfield ground
            if status['le'] == 0 or status['le'] == 2:
                continue
            # Do not process before a given delay
            if abs(next_fix['tsp'] - status['t']) < STATUS_DELAY:
                continue
            geor = red.georadius(name="GEO:L", longitude=next_fix['lng'], latitude=next_fix['lat'],
                                 radius=RADIUS, unit="km", withdist=True, count=1, sort='ASC')
            status['c'] = None
            status['t'] = next_fix['tsp']
            status['le'] = 3
            geocode = None if len(geor) == 0 else geor[0][0]
            if geocode is not None:
                if abs(next_fix['alt'] - codes[geocode][0]) < L_ALT:
                    status['c'] = geocode
                    status['le'] = 2
                    red.hset('FLEET:{}'.format(geocode), address, next_fix['tsp'])

        elif not prev_fix['low_speed'] and next_fix['low_speed']:
            # windy day, avoid double landing detection
            if status['le'] == 0 and abs(next_fix['tsp'] - status['t']) < 20:
                continue
            geor = red.georadius(name="GEO:L", longitude=next_fix['lng'], latitude=next_fix['lat'],
                                 radius=RADIUS, unit="km", withdist=True, count=1, sort='ASC')
            qfu = round((buf[0]['track'] + prev_fix['track'])/20)
            l_code = None if len(geor) == 0 else geor[0][0]
            if l_code is None:
                db.insert_event(timestamp=next_fix['tsp'], address=address, event=0, code=l_code,
                                tzidx=None, max_alt=status['ml'], qfu=qfu)
                evt = ("Landing {}, {} @ {}".format(address, datetime.fromtimestamp(next_fix['tsp']), l_code))
                log_evt(evt, buf)
            else:
                if abs(next_fix['alt'] - codes[l_code][0]) < L_ALT:
                    db.insert_event(timestamp=next_fix['tsp'], address=address, event=0, code=l_code,
                                    tzidx=codes[l_code][1], max_alt=status['ml'], qfu=qfu)
                    red.hset('FLEET:{}'.format(l_code), address, next_fix['tsp'])
                    red.xadd(name='SEVT', fields={l_code: 0}, approximate=True, maxlen=8)
                    status['ml'] = 0
                    status['le'] = 0
                    status['c'] = l_code
                    status['t'] = next_fix['tsp']
                    evt = ("Landing {}, {} @ {}".format(address, datetime.fromtimestamp(next_fix['tsp']), l_code))
                    log_evt(evt, buf)

        elif prev_fix['low_speed'] and not next_fix['low_speed']:
            # windy day, avoid double takeoff detection
            if status['le'] == 1 and abs(next_fix['tsp'] - status['t']) < 20:
                continue

            geor = red.georadius(name="GEO:L", longitude=next_fix['lng'], latitude=next_fix['lat'],
                                 radius=RADIUS, unit="km", withdist=True, count=1, sort='ASC')
            qfu = round((prev_fix['track'] + next_fix['track']) / 20)
            t_code = None if len(geor) == 0 else geor[0][0]
            if t_code is None:
                db.insert_event(timestamp=next_fix['tsp'], address=address, event=1, code=t_code,
                                tzidx=None, max_alt=None, qfu=qfu)
                evt = ("TakeOff {}, {} @ {}".format(address, datetime.fromtimestamp(next_fix['tsp']), t_code))
                log_evt(evt, buf)
            else:
                if abs(next_fix['alt'] - codes[t_code][0]) < L_ALT:
                    db.insert_event(timestamp=next_fix['tsp'], address=address, event=1, code=t_code,
                                    tzidx=codes[t_code][1], max_alt=None, qfu=qfu)
                    red.hset('FLEET:{}'.format(t_code), address, next_fix['tsp'])
                    red.xadd(name='SEVT', fields={t_code: 1}, approximate=True, maxlen=8)
                    status['ml'] = 0
                    status['le'] = 1
                    status['c'] = t_code
                    status['t'] = next_fix['tsp']
                    evt = ("TakeOff {}, {} @ {}".format(address, datetime.fromtimestamp(next_fix['tsp']), t_code))
                    log_evt(evt, buf)
        else:
            continue

    # keep last status values in redis for next run
    red.hset(name='STS', key=address, value=json.dumps(status))
    # create/append associated stream
    fields = dict()
    if len(polyline) > 0:
        fields['poly'] = json.dumps(polyline)
        fields['tsp'] = last_tsp
        red.xadd(name=stream, fields=fields, maxlen=300, approximate=True)
