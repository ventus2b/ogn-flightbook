## Production setup
#### Serveur Provisioning:
Flightbook was developed for small architecture constraints. Any machine with at least 1 CPU and 2G RAM is enough.  
You can try to allocate an Amazon EC2 t2.small instance, Digital Ocean Basic Droplet and so on ...  
Ensure than server firewall is setup to allow TCP port 22 (ssh) and TCP port 80 (http).  
Deployment has been planned for the latest Debian or Ubuntu distributions but it can fit with any up to date Linux system.  
You need docker, ansible, git and ssh client on your machine for deployment setup.  
Code portability for Windows OS is not considered.

#### Deploy
Once your server is working and your ssh keys are set, type on your machine:
```bash
git clone https://gitlab.com/lemoidului/ogn-flightbook.git
cd ogn-flightbook/deploy
ansible-playbook playbook_debian10.yml -i <server_name>, --user=<server_user_name>
                    or
ansible-playbook playbook_ubuntu20.04.yml -i <server_name>, --user=<server_user_name>
```
Produce the web app on your local system:
```bash
cd ../web
./build.sh
```
Copy the web app dist directory on server:
```bash
scp -r app/dist <server_user_name>@<server_name>:/home/<server_user_name>/ogn-flightbook/web/app
```
Produce the golang binary on your local system (websocket server):
```bash
cd ../wserv
./build.sh
```
Copy the binary on server:
```bash
scp wserv <server_user_name>@<server_name>:/home/<server_user_name>/ogn-flightbook/wserv/
```

#### Setting up
Login to server
```bash
ssh <server_user_namer>@<server_name>
```
Ensure than Redis Server is up and running
```bash
sudo systemctl redis-server start
sudo systemctl status redis-server
```
Populate devices database & airfields locations + timezone:
```bash
cd /home/<server_user_name>/ogn-flightbook
source ./venv/bin/activate
python initdb.py -ad
python initdb.py -acs
# you can watch possible cmd line options with:
python initdb.py --help
```
#### start services
Start Wsgy server (Gunicorn) & Nginx
```bash
sudo systemctl start gunicorn
sudo systemctl start nginx
# ensure that everything is ok with
sudo systemctl status gunicorn
sudo systemctl status nginx
```
Inside four differents tmux sessions, start micro-services: aprs, feed, consumer and websocket server:
```bash
tmux
source venv/bin/activate
python tasks.py aprs
# switch to another tmux Windows with [CTRL] + b then c:
source venv/bin/activate
python tasks.py feed
# switch to another tmux Windows with [CTRL] + b then c:
source venv/bin/activate
python tasks.py cons
# switch to another tmux Windows with [CTRL] + b then c:
./wserv/wserv
# detach from tmux with [CTRL] + b then d
```
That's all folks

## Development set up
```bash
git clone https://gitlab.com/lemoidului/ogn-flightbook.git
cd ogn-flightbook

```
#### Python
Install virtualenv and setup your virtual environment
```bash
# on Debian
sudo apt-get install python-virtualenv
# on Ubuntu
sudo pip3 install virtualenv
# Once virtualenv is installed
virtualenv -p python3 ./venv
source venv/bin/activate
pip install -r requirements
deactivate
```
#### Sqlite
Install some usefull tools: SqliteBrowser (GUI Db browser) and sqlite3 (CLI).
```bash
sudo apt-get install sqlitebrowser
sudo apt-get install sqlite3
```
#### Redis
Hacker, when you wake up in the morning don't forget to thank heaven for giving us Salvatore Sanfilippo aka [Antirez](http://antirez.com/latest/0)  
If you don't want to permanently install a redis instance on your development machine, docker is your friend:
```bash
# for non persistent storage in container
docker run --name myredis -d -p 127.0.0.1:6379:6379 redis
# for persistent storage
docker run --name myredis -d -p 127.0.0.1:6379:6379 redis redis-server --appendonly yes
# You can access redis-cli inside container with
docker exec -it myredis /bin/bash
redis-cli
```
If you don't worry to have a redis instance on your machine (everbody should have one !!).  
Package comes with cli.
```bash
sudo apt-get install redis-server
```

#### Nodejs and friends ...
The world of nodejs and the surrounding ecosystem with all these dependencies can be quite sickening. No Troll here just an informed statement.  
Also I strongly recommend to install your nodejs environment through [nvm](https://github.com/nvm-sh/nvm) (node ​​version manager).  
This will allow to: 
 - Easily choose a recent version of nodejs (the packaged versions are not necessarily up to date including on recent distributions)
 - be able to install as a non-root user
 - clean up by just deleting a directory before it all becomes a bloody mess.
 <!-- end of the list --> 
Differents install methods are well described in nvm github repo.  
Once nvm is installed & stable recent version of nodejs is set:
 ```bash
cd web/app
npm install
# live developpement
npm run serve 
# or create the chunkjs for production
npm run build
```
#### Golang
```bash
# */Start Install
# read https://golang.org/doc/install or use following cmds
curl https://dl.google.com/go/go1.15.6.linux-amd64.tar.gz -o /tmp/go1.15.6.linux-amd64.tar.gz
sudo tar -C /usr/local -xzf /tmp/go1.15.6.linux-amd64.tar.gz
#  adding the following line to your $HOME/.profile or /etc/profile (for a system-wide installation):
export PATH=$PATH:/usr/local/go/bin
# changes made to a profile may not apply until the next time you log into your computer, to apply immediatly:
source $HOME/.profile
# Verify installation
go version
# End Install */

cd wserv
go mod init flightbook/wserv
# for running without produce binary
go run *.go
# to produce binary
go build
./wserv

```

## Architecture overview
![Diag0](./pics/Global_Arch.png)

## Architecture in depth
Redis is the central piece of architecture.    
If you are new to Redis, you can read following docs:
 
 - [Redis data-types](https://redis.io/topics/data-types)
 - [Redis Geocoding](https://redis.io/commands/geoadd)
 - [Redis Stream](https://redis.io/topics/streams-intro)


### Redis as a Queue Manager
![Diag1](./pics/Diag1.png)
### Redis for Geocoding
#### Find closest Airfield:
![Diag2](./pics/Diag2.png)
#### Find surrounding beacons:
![Diag3](./pics/Diag3.png)
### Redis for streaming Data
TODO

## Scalability in mind
```
TODO
```

### Customize configuration
```
TODO
```

